## How can Chemical mattress affect your health?
 ##
Chemicals, of any kind, are believed to adversely affect the health of a living being.  It has been seen that nowadays, many mattresses do contain toxic chemical materials such as antimony, formaldehyde and flame retardants which have affected many lives. 
In this article, I am going to discuss with you that how the chemical materials present in the mattress can affect your health:

### 1.	Foam Toxic ###

Foam toxic is made up of Polyurethane Foam and Memory Foam which is petroleum based products. These materials break into major off-gassing of VOCs into the air you breathe. 
Some of the adverse effects of the foam toxins on your health are:
a)	Mammary tumors: 
Studies have shown that Propylene oxide and TDI do cause mammary tumors.
b)	Effect to the neurological system
Toluene gases are the byproduct of polyurethane foam products which is a neuron-toxin.
c)	Sources of several hazardous air pollutants
Methylene chloride, toluene diisocyanate (TDI), and hydrogen cyanide are some of the foam fabrication materials used by many industries which are the sources of several air pollutants.
d)	Irritation in the eyes, nose, and throat
e)	Headaches, loss of coordination, and nausea,
f)	Causes damage to the liver, kidney, and central nervous system.
g)	The foam is extra flammable and contains chemical flame retardants.


## 2.	 Synthetic Latex Toxic ##

Synthetic latex toxins are made of petroleum-based by-products such as styrene and butadiene. These petroleum-based by-products can pose serious health issues.
Exposure to Butadiene can cause several problems, some of which are:
a)	Adverse effect on the nervous system
b)	Irritation in eyes and skin
c)	In rare cases, it may even cause cancer.

The other product styrene is believed to have serious health consequences which are:
a)	Lack of brain coordination
b)	Mental retardation and confusion
c)	Effect on central nervous system and neurons
d)	Loss of hearing capability
e)	Vision impairment
f)	Increased risk of other diseases such as lymphoma or leukemia

Some companies make use of synthetic ingredients at first and later on add some natural latex to it. Then, they will mark the product as “natural latex.” You must pay heed to complete making and raw materials used in making of the mattress.

### 3.	Chemical Flame Retardants Toxic ###

These retardants are not at first blended in the making of the mattress; instead, these retardants are released over the time into the air. Some of the chemical flame retardants include Antimony, Boric Acid, and Halogenated Flame Retardants which are believed to pose several health issues that can span generations to come.
Some of the effects of these toxic gases are:
a)	Disruption of thyroid hormones
b)	Delay in reproductive development of the body, puberty and some other changes to neurological behavior.
c)	Cause harm to the layers of brain and functioning of nervous system
d)	These retardants are seen to harm sperm and their mobility, even after for coming generations.

### 4.	Vinyl Toxic ###

Phthalates and heavy metals are the additives used in making Vinyl. These can cause the following health problems:
a)	Genital defects
b)	Asthma and allergy
c)	In children, it may cause delay in development of brain

You must make an informed decision while choosing a mattress for you.

### [Chemical Free Mattress](https://www.naturalmattressmatters.com/chemical-free-mattress/) ###****